import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

msg = b"Hello world..."

s.sendto(msg, ("localhost", 9004))

data, addr = s.recvfrom(100000)

print(data)