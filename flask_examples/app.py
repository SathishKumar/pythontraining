from flask import Flask, request, redirect, url_for, render_template

app = Flask(__name__)

@app.route('/<name>/', methods = ['GET'])
def hello_world(name):
    return "Hello "+name

@app.route("/add/")
def add():
    return str(int(request.values['v1']) + int(request.values['v2']))

@app.route('/add_alias/')
def add_alias():
    return redirect(url_for('add', v1=10, v2=20))

@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template("form.html")
    elif request.method == 'POST':
        return "Done"


if __name__ == '__main__':
    app.run(debug=True)