import os, sys, time

x = 9

r, w = os.pipe()

pid = os.fork()

# def handler(a,b):
#     print("Child done its work")

if pid == 0:
    os.close(r)
    w = os.fdopen(w, 'w')
    print("Child writing..")
    time.sleep(2)
    w.write('Child writes this')
    w.close()
    print("Done")
    # # os.execlp('ls', 'ls')
    # print("I am a child")
    # print("parent id is", os.getppid())
else:
    os.wait()
    os.close(w)
    r = os.fdopen(r)
    print("Parent reading")
    time.sleep(2)
    mstr = r.read()
    print("text = ", mstr)
    sys.exit(0)
    # signal.signal(signal.SIGCHLD, handler)
    # print("I am parent", os.getpid())