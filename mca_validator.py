model_file = open('model.csv', 'r')
model_ans = list(map(lambda x: tuple(x.strip().split(',')), model_file.readlines()))
model_file.close()

answer_file = open('answer.csv', 'r')
answers =list(map(lambda x: tuple(x.strip().split(',')), answer_file.readlines()))
answer_file.close()

marks = list(map(lambda x,y: 1 if x == y else(0 if y[1] == 'x' else -1), model_ans, answers))


print("You have scored -", sum(marks))
print("Not answered -", marks.count(0))
print("Wrong answers -", marks.count(-1))


# print("You scored:",len(model_ans.intersection(answers)))