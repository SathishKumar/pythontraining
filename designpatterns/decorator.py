# class MyDecorator:
#     def __init__(self, f):
#         print("Logging into the function")
#         f()
#     def __call__(self, *args, **kwargs):
#         print("Logging out of the functon")

def loggingF(func):
    import logging
    logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
    def new_f():
        logging.info("Entering into the fun")
        func()
        logging.info("Exited")
    return new_f

import logging
@loggingF
def myfun():
    logging.info("Entering into the function")
    for i in range(10):
        print(i)

myfun()
