class Singliton(object):
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singliton, cls).__new__(cls)
        return cls.instance


class Borg:
    __shared_state = {}
    def __init__(self):
        self.x = 1
        self.__dict__ = self.__shared_state
        pass

b = Borg()
b1 = Borg()

print(b.__dict__)
print(b1.__dict__)

b1.x = 20

print(b.__dict__)
print(b1.__dict__)

