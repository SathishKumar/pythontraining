from sklearn.preprocessing import Imputer, LabelEncoder, OneHotEncoder
import numpy as np
import pandas as pd


# data = pd.read_csv("sampledata.csv")
# imr = Imputer(missing_values='NaN', strategy='mean', axis=1)
# imr = imr.fit(data)
# imputed_data = imr.transform(data)
# print(imputed_data)

df = pd.DataFrame([
    ['green', 'M', 10.1, 'class1'],
    ['blue', 'L', 13.5, 'class2'],
    ['red', 'XL', 15.3, 'class1'],
])
df.columns = ['color', 'size', 'price', 'classlabel']
size_mapping = {'XL':3, 'L':2, 'M':1}
df['size'] = df['size'].map(size_mapping)
class_mapping = {label:idx for idx, label in enumerate(np.unique(df['classlabel']))}
df['classlabel'] = df['classlabel'].map(class_mapping)

x = df[['color', 'size', 'price']].values
color_le = LabelEncoder()
x[:,0] = color_le.fit_transform(x[:,0])
# print(x)
ohe = OneHotEncoder(categorical_features=[0])
# print(ohe.fit_transform(x).toarray())


ex = pd.DataFrame([0,1,2,3,4,5])
ex[1] = (ex[0] - ex[0].mean())/ex[0].std(ddof=0)
ex[2] = (ex[0] - ex[0].min())/(ex[0].max() - ex[0].min())
ex.columns = ['input', 'standardized', 'normalized']
print(ex)
