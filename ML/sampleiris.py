from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.linear_model import Perceptron, LogisticRegression
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# iris = load_iris()
# x = iris.data
# y = iris.target

# print(train_test_split(X,Y, test_size=0.3))
df_wine = pd.read_csv("https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data", header=None)
y=df_wine.iloc[:,0].values
x=df_wine.iloc[:,1:].values

df_wine.columns = ['class label', 'Alcohol', 'Malic acid', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols', 'Flavanoids', 'Nonflavanoid phenols', 'Proanthocyanins', 'Color intensity', 'Hue', 'OD280/OD315 of diluted wines', 'Proline']

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
sc = StandardScaler()
x_train_std = sc.fit_transform(x_train)
x_test_std = sc.fit_transform(x_test)

# ppn = Perceptron(max_iter=20, eta0=0.1)
ppn = LogisticRegression(penalty='l1', C=1.0)
ppn.fit(x_train_std, y_train)
y_pred = ppn.predict(x_test_std)
print("Misclassification testing", (y_test - y_pred).sum())

colors = ['red', 'blue', 'black', 'green', 'orange', 'yellow', 'purple', 'pink', 'indigo', 'brown', 'gray', 'lightgreen', 'lightblue']

weights, params = [], []
for c in np.arange(-4., 6.):
    lr = LogisticRegression(penalty='l1', C=10.**c, random_state=0)
    lr.fit(x_train_std, y_train)
    weights.append(lr.coef_[1])
    params.append(10.**c)

weights = np.array(weights)

for column, color in zip(range(weights.shape[1]), colors):
    plt.plot(params, weights[:, column], label=df_wine.columns[column + 1], color = color)

plt.xlim([10**(-5), 10**5])
plt.xscale('log')
plt.legend(loc='best')
plt.axhline(0, color='black', linestyle='--', linewidth=3)

plt.show()
