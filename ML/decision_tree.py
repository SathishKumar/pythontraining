from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.datasets import load_iris
import pydotplus
from IPython.display import Image, display


iris = load_iris()
x = iris.data
y = iris.target

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
ppn = DecisionTreeClassifier(max_depth=2)
fit = ppn.fit(x_train, y_train)
y_train_pred = ppn.predict(x_test)

print("Misclassification", (y_test - y_train_pred).sum())

dot_data = tree.export_graphviz(fit, out_file=None, feature_names= iris.feature_names, class_names= iris.target_names, filled=True, rounded=True)

graph = pydotplus.graph_from_dot_data(dot_data)
print("-----")
print(display(Image(data=graph.create_png())).__doc__)