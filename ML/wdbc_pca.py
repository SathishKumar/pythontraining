import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression

df = pd.read_csv('wdbc.data', header=None)
X = df.loc[:,2:].values
Y = df.loc[:,1].values
l = LabelEncoder()
Y = l.fit_transform(Y)
print(Y)
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.3)

pipe = Pipeline([('sc', StandardScaler()),
                 ('pca', PCA(n_components=5)),
                 ('lr', LogisticRegression(penalty='l1'))
                 ])

pipe.fit(X_train, Y_train)
print("Test accuracy", pipe.score(X_test, Y_test))
