import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.metrics import mean_squared_error, r2_score

df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/housing/housing.data', header=None, sep='\s+')
df.columns= ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS', 'RAD', 'TAX', 'PTRATIO', 'B', 'LSTAT', 'MEDV']
sns.set(style='whitegrid', context='notebook')
cols = ['RM','TAX','PTRATIO','LSTAT','MEDV']

# sns.pairplot(df[cols], size=2.5)
# plt.tight_layout()

# import numpy as np
#
# cm = np.corrcoef(df[cols].values.T)
# sns.set(font_scale=1.5)
# hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f')
#
# X = df[['RM']].values
# Y = df['MEDV'].values
#
# from sklearn.linear_model import LinearRegression
# def lin_regplot(X,Y,slr):
#     plt.scatter(X,Y,c='blue')
#     plt.plot(X,slr.predict(X), color='red')
#     return None
#
# slr = LinearRegression()
# slr.fit(X,Y)
# print('Slope: %.3f' % slr.coef_[0])
# print('Intercept: %.3f' % slr.intercept_)
#
# # lin_regplot(X, Y, slr)
#
# # plt.xlabel('RM')
# # plt.ylabel('Price')


X = df.iloc[:, :-1].values
Y = df['MEDV'].values

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=0)
ridge = Ridge(alpha=0.1)
slr = LinearRegression()

slr.fit(x_train, y_train)
y_train_pred = slr.predict(x_train)
y_test_pred = slr.predict(x_test)

print('MSE train: %.3f, test: %.3f' % (mean_squared_error(y_train, y_train_pred), mean_squared_error(y_test, y_test_pred)))

print('r^2 train: %.3f test: %.3f' % (r2_score(y_train, y_train_pred), r2_score(y_test, y_test_pred)))

plt.scatter(y_train, y_train_pred - y_train, c='blue', marker='o', label="training data")
# plt.scatter(y_test_pred, y_test_pred - y_test, )

plt.show()