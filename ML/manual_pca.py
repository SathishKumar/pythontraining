from numpy import array, mean, cov
from numpy.linalg import eig
from sklearn.decomposition import PCA

a = array([[1,4],[3,2],[5,9]])
print(a)

# M = mean(a.T, axis=1)
# print(M)
#
# C = a - M
# print(C)
#
# V = cov(C.T)
# print(V)
#
# values, vectors = eig(V)
# print(values)
# print(vectors)
#
# P = vectors.T.dot(C.T)
# print(P.T)

pca = PCA(2)

pca.fit(a)

print(pca.components_)
print(pca.explained_variance_)

B = pca.transform(a)
print(B)