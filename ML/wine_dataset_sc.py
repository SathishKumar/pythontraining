from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.linear_model import Perceptron, LogisticRegression
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.linear_model import Perceptron, LogisticRegression


# iris = load_iris()
# x = iris.data
# y = iris.target

# print(train_test_split(X,Y, test_size=0.3))
# df_wine = pd.read_csv("https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data", header=None)
# y=df_wine.iloc[:,0].values
# x=df_wine.iloc[:,1:].values
iris = load_iris()
x = iris.data
y = iris.target
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
sc = StandardScaler()
x_train_std = sc.fit_transform(x_train)
x_test_std = sc.fit_transform(x_test)

pca = PCA(3)
x_train_pca = pca.fit_transform(x_train_std)
x_test_pca = pca.fit_transform(x_test_std)
print(x_test_pca)

ppn = LogisticRegression(penalty='l1', C=1.0)
ppn.fit(x_train_pca, y_train)
y_pred = ppn.predict(x_test_pca)
print("Misclassification testing", (y_test - y_pred).sum())