# import doctest
import unittest

class Myclass:
    pass

def myfunc(obj):
    # '''
    # >>> myfunc(Myclass()) #doctest: +ELLIPSIS
    # <__main__.Myclass object at 0x...>
    # '''
    return obj

class MyTest(unittest.TestCase):
    def testobj(self):
        self.assertIsInstance(Myclass(), Myclass)

if __name__ == '__main__':
    # doctest.testmod(verbose=True)
    unittest.main()