def add(a,b):
    return a+b

def sub(a,b):
    return a-b

import unittest

class MathTest(unittest.TestCase):
    def testadd(self):
        # self.assertTrue(add(2,3) == 5)
        self.assertEqual(add(5,6), 11)
    # def testsub(self):
    #     self.assertTrue(sub(4,2) == 2)


class NewMathTest(unittest.TestCase):
    def testsub(self):
        self.assertTrue(sub(4,2) == 2)


if __name__ == '__main__':
    suit = unittest.TestLoader().loadTestsFromTestCase(MathTest)
    unittest.TextTestRunner(verbosity=3).run(suit)

# if __name__ == '__main__':
#     print(add(10,8))
#     print(sub(8,3))


