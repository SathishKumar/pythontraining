import pdb

num = int(input("Enter the number: "))

def findvalue(num):
    value = 0
    for i in range(1, num):
        value = value + 2*i
        value = value * value
        value = value / i*i

    if value > 100:
        print("Large value", value)

    elif value == 100:
        print("Equal to 100")

    else:
        print("value is small", value)

pdb.runcall(findvalue, num)