import doctest

def my_func(a, b):
    """
    >>> my_func(2,3)
    6
    >>> my_func('a',5)
    'aaaaa'
    """
    return a * b

def dividedbyzero():
    """
    >>> dividedbyzero()
    Traceback (most recent call last):
    ZeroDivisionError: division by zero
    """
    return 10/0

if __name__ == '__main__':
    doctest.testmod(verbose=True)