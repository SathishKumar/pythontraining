import pdb

def somediv(some_int):
    print("Start int,",some_int)
    print("Start int,", some_int)
    ret_int = 10/some_int
    pdb.set_trace()
    print("End some int", some_int)
    return ret_int

# print(pdb.runeval("somediv(2)"))
# print(pdb.runcall(somediv, 2))

try:
    somediv(0)
except:
    import sys
    tb = sys.exc_info()[2]
    pdb.post_mortem(tb)

