import tarfile

# for filename in ['HINTS', 'fileIO.py',
#                  'newprogram.tar', 'notthere.tar']:
#     try:
#         print('{:>15}  {}'.format(filename, tarfile.is_tarfile(
#             filename)))
#     except IOError as err:
#         print('{:>15}  {}'.format(filename, err))

with tarfile.open('newprogram.tar', 'r') as t:
    mem = t.extractfile('iters.py')
    data = mem.readlines()

print(data)