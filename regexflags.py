import re

# greet = "Hello\nWorld"
#
# pat1 = re.compile('.+', re.DOTALL)
#
# print(pat1.search(greet))
#
# pat2 = re.compile('.+', re.MULTILINE)
#
# print(pat2.search(greet))

string = "Hello, this is example, for split using re"

p = re.compile(r'\W+')

li = p.split(string, 1)

print(li)

string2 = 'blue socks and black shoes'

p2 = re.compile('(blue|red|black)')

s = p2.sub('color', string2)

print(s)