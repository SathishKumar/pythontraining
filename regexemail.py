import re

emailpat = re.compile('([a-z0-9])@(.+)\.(.+)')

mail = input("Enter your email: ")

mat = emailpat.search(mail)

if mat:
    print(mat.group(0))
    print(mat.group(1))
    print(mat.group(2))
    print(mat.group(3))
else:
    print("No match found")