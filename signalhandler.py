import signal
import time

def handler(a,b):
    print("Dont works here")
    signal.signal(signal.SIGINT, signal.SIG_DFL)

# signal.signal(signal.SIGINT, handler)
#
# while True:
#     print("Ctrl + c never works here")
#     time.sleep(1)

def alarm_handler(a,b):
    print("Alarm raised", time.ctime())

signal.signal(signal.SIGALRM, alarm_handler)

signal.alarm(4)
print("current time", time.ctime())
time.sleep(6)