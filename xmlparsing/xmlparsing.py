from xml.sax.handler import ContentHandler
from xml.sax import make_parser

class MenuHandler(ContentHandler):
    def startElement(self, name, attrs):
        if (name == 'food'):
            self.name = attrs.get("id")

    def endElement(self, name):
        if (name == "food"):
            print("%s" % (self.name))


menu = MenuHandler()
saxparser = make_parser()
saxparser.setContentHandler(menu)
data = open("sample.xml", 'r')
saxparser.parse(data)
data.close()
