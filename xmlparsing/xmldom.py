from xml.dom import minidom

doc = minidom.parse("sample.xml")

food_name = doc.getElementsByTagName("name")[0]

# print(food_name.firstChild.data)

foods = doc.getElementsByTagName("food")

for food in foods:
    fid = food.getAttribute("id")
    food_name = food.getElementsByTagName("name")[0].firstChild.data
    price = food.getElementsByTagName("price")[0].firstChild.data
    description = food.getElementsByTagName("description")[0].firstChild.data
    calories = food.getElementsByTagName("calories")[0].firstChild.data
    print("\n")
    print("ID:", fid)
    print("Name:", food_name)
    print("Price:", price)
    print("Calories:", calories)
    print("Description:", description)
    print("-"*100)

