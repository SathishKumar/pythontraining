class fibnum:
    def __init__(self, limit):
        self.limit = limit
        self.f1 = 0
        self.f2 = 1
        self.counter = 0
    def __iter__(self):
        return self
    def __next__(self):
        if self.counter >= self.limit:
            raise StopIteration
        self.oldf = self.f1 + self.f2
        self.f1 = self.f2
        self.f2 = self.oldf
        self.counter += 1
        return self.oldf

class qsequence:
    def __init__(self, s):
        self.s = s[:]

    def __next__(self):
        try:
            q = self.s[-self.s[-1]] + self.s[-self.s[-2]]
            self.s.append(q)
            return q
        except IndexError:
            raise StopIteration()

    def __iter__(self):
        return  self

    def current_state(self):
        return self.s


# f = fibnum(10)
q = qsequence([1,2])
while True:
    try:
        print(next(q))
    except:
        print("End")
        break