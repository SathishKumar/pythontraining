import pickle
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

s.bind(('', 9004))

while True:
    data, add = s.recvfrom(10000)
    f = open('client_obj', 'wb')
    f.write(data)
    f.close()
    f = open('client_obj', 'rb')
    message = pickle.load(f)
    f.close()
    s.sendto(bytes(message.my_name.upper(), 'utf-8'), add)