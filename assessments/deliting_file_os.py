import os

pid = os.fork()

if pid == 0:
    print("child")
    with open("file_list.txt", 'w') as f:
        for file in os.listdir('.'):
            if os.stat(file).st_size == 0:
                f.write(file+"\n")
else:
    os.wait()
    with open("file_list.txt") as nf:
         file_list = nf.read().strip().split("\n")
         print(file_list)
         for file in file_list:
             os.unlink(file)


