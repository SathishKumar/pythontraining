import pickle
import socket
from class_name import NameClass
import doctest
import unittest


def send_data():
    f = open('Object', 'wb')
    pickle.dump(NameClass(), f)
    f.close()

    f = open('Object', 'rb')
    message = f.read()
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    msg = ''
    try:
        s.sendto(message, ("localhost", 9004))
        data, addr = s.recvfrom(100000)
        msg = str(data, 'utf-8')
        print(str(data, 'utf-8'))
    except:
        print("Error sending message")
    finally:
        f.close()
    return msg

class ClientTest(unittest.TestCase):
    def test_send_data(self):
        n = NameClass()
        self.assertTrue(send_data() == n.my_name.upper())

if __name__ == '__main__':
    suit = unittest.TestLoader().loadTestsFromTestCase(ClientTest)
    unittest.TextTestRunner(verbosity=3).run(suit)