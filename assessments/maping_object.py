
import re
class Student:
    def __init__(self):
        self.name = 'sathish'
        self.__dept = 'cse'
        self.__college = 'ucev'
        self.__email = 'sathish@gmail.com'
    def getemail(self):
        return self.__email
    def getcollege(self):
        return self.__college
    def getdept(self):
        return self.__dept
    new = 111
s1 = Student()
d = {}
for attr in dir(s1):
    if re.search('^[a-zA-Z]+', attr) and 'get' in attr:
        d.update({attr.replace('get', '') : s1.__getattribute__(attr)()})
    elif re.search('^[a-zA-Z]+', attr):
        d.update({attr:s1.__getattribute__(attr)})

print(d)
