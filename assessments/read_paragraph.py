import re



data = (l for l in open("sample.txt"))
paras = []
def add_data(s=''):
    global paras
    try:
        line = next(data)
        if re.match('^\\n{2,}$',line):
            paras.append(s)
            add_data(s)
        else:
            s+=line
            add_data(s)
    except:
        return s

print(add_data())


#
# with open("sample.txt") as f:
#     data = re.split(r'\n{2,}', f.read())