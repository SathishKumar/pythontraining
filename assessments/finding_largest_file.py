import os
import paramiko

files = os.listdir('.')
file_sizes = [os.stat(x).st_size if os.path.isfile(x) else 0 for x in files]

print(file_sizes)
print(max(file_sizes))
t = paramiko.Transport(('localhost', 22))
for f in files:
    if os.path.isfile(f) and os.stat(f).st_size == max(file_sizes):
        print(f, os.stat(f).st_size)
        t.connect(username='cisco', password='cisco')
        sftp = paramiko.SFTPClient.from_transport(t)
        sftp.put(f, '/home/cisco/Desktop/getting_largest_file/'+f)





