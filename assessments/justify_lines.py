new_file = open("alligned_sample.txt", 'w')
data = (line.strip() for line in open("sample.txt"))
justified_lines = []
for i in data:
    justified_lines.extend([i[start:start + 50] for start in range(0, len(i), 50)])

new_file = open("alligned_sample.txt", 'w')
new_file.writelines([j.strip().expandtabs(10)+"\n" for j in justified_lines])
new_file.close()