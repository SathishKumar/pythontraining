'''
converting list into matrix
'''

def givematrix(a, b):
    '''
    :param a: should be a list contains number of elements equal to the product of b
    :param b: should be a list or tuple contains only 2 elements
    :return: matrix of b[0]xb[1]
    '''
    if len(a) != b[0] * b[1]:
        return "Length should be equal to product of tuple"
    mat = []
    (s, e) = (0, b[1])
    for i in range(b[0]):
        mat.append(a[s:e])
        (s, e) = (s+b[1], e+b[1])
    return mat

print(givematrix([1,2,3,4,5,6,7,8,9,10,11,12], (3,4)))