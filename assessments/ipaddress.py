'''
Finding the next IP
'''

initial_ip = '255.255.255.255'
MAX = '255.255.255.255'

if initial_ip == MAX:
    print("This the max IP")

else:

    ip_list = initial_ip.split('.')
    ip_rev = ip_list[::-1]
    next_ip = '.'.join(list(map(lambda x: str(int(x)+1) if int(x) < 255 else '0', ip_rev))[::-1])
    print(next_ip)

