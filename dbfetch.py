import pymysql

db = pymysql.connect("localhost", "demo", "demo", "test")

cursor = db.cursor()

query = "UPDATE pets SET age = age + 1 WHERE age > 10"

try:
    cursor.execute(query)

except:
    db.rollback()

else:
    db.commit()

db.close()