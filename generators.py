# def BeerData():
#     f = open("recipeData.csv")
#     for line in f:
#         yield line
#
# beer = BeerData()
#
# print(next(beer))

# print(next(beer))
#
# print(next(beer))

li = [n**2 for n in range(10)]

print(li)

ge = (n*2 for n in range(10))

print(ge)

lines = (line for line in open("recipeData.csv"))

cols = (l.split(',') for l in lines)

columns = next(cols)
beerdicts = (dict(zip(columns, data)) for data in cols)


beer_counts = {}
while True:
    try:
        bd = next(beerdicts)
        if bd["Style"] in beer_counts:
            beer_counts[bd["Style"]] += 1
        else:
            beer_counts[bd["Style"]] = 1
    except:
        break

print(beer_counts)
# for bd in beerdicts:
#     if bd["Style"] not in beer_counts:
#         beer_counts[bd["Style"]] += 1
#     else:
#         beer_counts[i["Style"]] = 1
