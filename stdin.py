# fname = input("Enter the first name: ")
# lname = input("Enter the last name: ")
#
# print("First name is:", fname)
# print("Last name is:", lname)

import subprocess
import os

completed = subprocess.run(
    ['ls'],
    stdout=subprocess.PIPE,
)
# print('returncode:', completed.returncode)
# print('Have {} bytes in stdout:\n{}'.format(
#     len(completed.stdout),
#     completed.stdout.decode('utf-8'))
# )

flist = (completed.stdout.decode('utf-8')).split("\n")
flist.remove('')
size = 0
for fname in flist:
    size += os.stat(fname).st_size

print(size, "Bytes")