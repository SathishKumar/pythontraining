"""SampleProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
# from django.conf.urls.defaults import *
from MyAPP.views import *

urlpatterns = [
    # path('hello/<name>/',say_hello),
    path('change_pub_data/', change_pub_data),
    path('login_page/', login_page),
    path('login/', login),
    path('logout/', logout),
    path('test_cookie/', test_cookie),
    path('upload_page/', upload_page),
    path('upload_img/', upload_img),
    path('show_allcookies/', show_allcookies),
    path('book_form/', BookFormPage),
    path('get_color/', get_color),
    path('set_color/', set_color),
    path('search_page/', search_page),
    path('search_publisher/', search_publisher),
    re_path('hello/(\w+)/', say_hello),
    re_path('template/(\w+)/(\w+)/(\d+)/', template_ex),
    re_path('time/plus/(\d+)', get_timeahead),
    path('current_date/',current_datetime),
    path('admin/', admin.site.urls),
]
