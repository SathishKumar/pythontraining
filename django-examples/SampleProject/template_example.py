from django.template import Context, Template

names = ['sathish', 'jey', 'ezhil', 'vishnu']

t = Template('Hello, {{name}}')
for name in names:
    print(t.render(Context({'name':name})))