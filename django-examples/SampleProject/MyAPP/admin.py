from django.contrib import admin
from MyAPP.models import *
# Register your models here.

admin.site.register(Publisher)
admin.site.register(Author)
admin.site.register(Book)
