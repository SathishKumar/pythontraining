from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse
from django.template import Context, Template
from MyAPP.models import *
from MyAPP.ModelForms import *

# Create your views here.
def CheckAccess(func):
    def new_fun(request):
        if 'a' in request.COOKIES:
            func(request)
        else:
            return redirect('/login_page')
    # return new_fun
    return new_fun

def say_hello(request, name):
    return HttpResponse("Hello %s" % (name))

def current_datetime(request):
    import datetime
    time = datetime.datetime.now()
    return render_to_response('current_time.html', {'time':time})

def get_timeahead(request, offset):
    import datetime
    offset = int(offset)
    time_ahead = datetime.datetime.now() . datetime.timedelta(hours = offset)
    return HttpResponse("Time after %s hours is %s" % (offset, time_ahead))

class Person:
    def __init__(self, names, age):
        self.name = names
        self.age = age

class SilentAssertionError(AssertionError):
    silent_variable_failure = True

class PersonClass3:
    def first_name(self):
        raise SilentAssertionError


def template_ex(request, *args):
    t = Template("Hello {{person.name.0.upper}} your nick name is {{person.name.1.upper}} you are {{person.age}} yrs old")
    # person = {'name':args[0], 'age':args[1]}
    p = PersonClass3()
    # return HttpResponse(t.render(Context({'person':p})))

    return HttpResponse(t.render(Context({'person':Person([args[0], args[1]], args[2])})))

@CheckAccess()
def search_page(request):
    return render_to_response('search_page.html')

@CheckAccess()
def search_publisher(request):
    message = ''
    publisher_data = ''
    if 'q' in request.GET:
        publisher_list = Publisher.objects.filter(name=request.GET['q'])
        if publisher_list:
            publisher_data = publisher_list.values()[0]
            print(publisher_data)
        else:
            message = "No matching publisher found"
    else:
        message = "You have submitted empty form"
    return render_to_response('search_page.html', {'message':message, 'publisher_data' : publisher_data})

def change_pub_data(request):
    name = request.GET['name']
    address = request.GET['address']
    city = request.GET['city']
    state = request.GET['state']
    country = request.GET['country']
    publisher_data = Publisher.objects.get(name=name)
    message = ''
    if publisher_data:
        print(publisher_data.name)
        publisher_data.name = name
        publisher_data.address = address
        publisher_data.city = city
        publisher_data.state_province = state
        publisher_data.country = country
        publisher_data.save()
        message='Update Done!'
    else:
        message = "Invalid data entered"
    return render_to_response('search_page.html', {'message': message, 'publisher_data': ''})


def BookFormPage(request):
    form = BookForm()
    print(form)
    return render_to_response('book_form.html', {'form':form})

def get_color(request):
    return render_to_response('set_cookie.html')

def set_color(request):
    if 'fav_color' in request.GET:
        response = HttpResponse('Your fav color is %s' %(request.GET['fav_color']))
        response.set_cookie("fav_color", request.GET['fav_color'])
        return response
    else:
        return render_to_response('set_cookie.html')

@CheckAccess()
def show_allcookies(request):
    cookies = []
    for cookie in request.COOKIES:
        cookies.append({cookie:request.COOKIES[cookie]})

    return HttpResponse(cookies)

def test_cookie(request):
    if request.session.test_cookie_worked():
        return HttpResponse("Cookie enabled in your browser")
    else:
        return HttpResponse("Cookie disabled in your browser")


def login_page(request):
    if 'fav_color' in request.COOKIES:
        bgcolor = request.COOKIES['fav_color']
        response = render_to_response('login_page.html', {'bgcolor': bgcolor})
    else:
        bgcolor = 'white'
        response = render_to_response('login_page.html', {'bgcolor':bgcolor})
        response.set_cookie('fav_color', 'white')
    return response

def login(request):
    import os
    import re
    f = open(os.path.abspath("MyAPP/credentials.txt"), 'r')
    data = f.readlines()
    f.close()
    # user_name = data[0]
    # password = data[1]
    flag = 0
    for i in data:
        cr = i.split(":")
        if request.GET['uname'] == cr[1] and request.GET['password'] == cr[2]:
            response = redirect('/search_page')
            if re.search('^admin', cr[0]):
                response.set_cookie('a', 1)
            else:
                response.set_cookie('u', 1)
            flag = 1
            break

    if flag == 1:
        return response
    else:
        return HttpResponse("Invalid login!")


def logout(request):
    response = redirect('/login_page')
    if 'u' in request.COOKIES:
        response.delete_cookie('u')
    else:
        response.delete_cookie('a')
    response.delete_cookie('password')
    return response

@CheckAccess()
def upload_page(request):
    return render_to_response("upload_page.html")

from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
@CheckAccess()
def upload_img(request):
    f = request.FILES['pic']
    with open(f.name, 'wb') as fn:
        for chunk in f.chunks():
            fn.write(chunk)
    # ctype = f.content_type.split('/')[0]
    # with open(f.name+"."+ctype, 'wb') as up:
    #     for chunk in f.chunks():
    #         up.write(chunk)

    return HttpResponse("Done")