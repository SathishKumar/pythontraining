from MyAPP.models import *
from django.forms import ModelForm

class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = ['title', 'authors', 'publisher', 'publication_date']

