import _thread

import time

def print_time(delay):
    while 1:
        time.sleep(delay)
        print(time.ctime(time.time()))

def print_name(name):
    while 1:
        time.sleep(2)
        print("Your name is", name)

_thread.start_new_thread(print_time, (2,))

_thread.start_new_thread(print_name, ('Sathish',))

while True:
    pass