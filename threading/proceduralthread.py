import threading
import time
loops = [4,2]

def loop(nloop, nsec):
    print("start loop", nloop, 'at:', time.ctime(time.time()))
    time.sleep(nsec)
    print("loop", nloop, "done at:", time.ctime(time.time()))

def main():
    print("Starting threads..")
    threads = []
    nloops = range(len(loops))
    for i in nloops:
        t = threading.Thread(target=loop, args=(i, loops[i]))
        threads.append(t)
    for i in nloops:
        threads[i].start()
    for i in nloops:
        threads[i].join()
    print("All done at:", time.ctime(time.time()))

if __name__ == '__main__':
    main()

