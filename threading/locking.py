import _thread
import time

def myfuncton(string, sleeping, lock, *args):
    while 1:
        lock.acquire()
        print(string, "Now sleeping after lock acquire for", sleeping)
        time.sleep(sleeping)
        print(string, "Now releasing lock and sleeping again")
        lock.release()
        # time.sleep(sleeping)

if __name__ == '__main__':
    lock = _thread.allocate_lock()
    _thread.start_new_thread(myfuncton, ("T1",2,lock))
    _thread.start_new_thread(myfuncton, ("T2", 2, lock))

    while 1:
        pass