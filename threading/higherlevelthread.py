import threading, time

class PrintTime(threading.Thread):
    def __init__(self, interval):
        threading.Thread.__init__(self)
        self.interval = interval

    def run(self):
        while 1:
            time.sleep(self.interval)
            print("I am a higher level thread")

p1 = PrintTime(2)
#p1.start()

while True:
    pass

