class Repeater:
    def __init__(self, v, uplim):
        self.value = v
        self.limit = uplim
        self.count = 0
    def __iter__(self):
        return self
    def __next__(self):
        if self.count >= self.limit:
            raise StopIteration
        self.count += 1
        return self.value

# class RepeatItrator:
#     def __init__(self, source):
#         self.source = source
#     def __next__(self):
#         return self.source.value

r1 = Repeater([1,2,3], 5)

itervalues = r1.__iter__()
print(next(itervalues))
print(next(itervalues))
print(next(itervalues))
print(next(itervalues))
print(next(itervalues))
print(next(itervalues))