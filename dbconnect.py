
import pymysql

db = pymysql.connect("localhost", "demo", "demo", "test")
#
cursor = db.cursor()
#
# cursor.execute("SELECT VERSION()")
#
# data = cursor.fetchone()

# query = "INSERT INTO pets VALUES ('%s', '%s', '%d')" % ('Jack', 'Mano', 20)
query = "SELECT * FROM pets;"
try:
    cursor.execute(query)
    data = cursor.fetchall()
except:
    db.rollback()
else:
    db.commit()

print(data)
db.close()

