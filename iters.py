print("Iter list")
ilist = iter([1,2,3,4])

print(next(ilist))
print(next(ilist))
print(next(ilist))
print(next(ilist))

print("iter set..")
iset = iter({'a','b','c'})

print(next(iset))
print(next(iset))
print(next(iset))

print("iter string..")
istr = iter("Hello world")

print(next(istr))
print(next(istr))
print(next(istr))
print(next(istr))

print("Iter dict")
d = {'a':10, 'b':20, 'c':30}
idict = iter(d.items())

print(next(idict))
print(next(idict))
print(next(idict))