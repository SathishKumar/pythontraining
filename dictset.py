def couple(a,b,u):
    d = {}
    for i in u:
        if i in a and i in b:
            d[i] = [a[i], b[i]]
        elif i in b:
            d[i] = b[i]
        elif i in a:
            d[i] = a[i]
    return d


a = {'a':5, 't':3, 'c':4, 'e':7}
b = {'a':3, 'l':9, 'e':1, 'd':6}

print("Union is", couple(a, b, set(a).union(set(b))))
print("Intersection is", couple(a, b, set(a).intersection(set(b))))
print("Sub is", couple(a, b, set(a)-set(b)))
print("Symmetric diff is", couple(a, b, set(a).symmetric_difference(b)))
print("Is subset", set(a).issubset(b))

