import os
import time
import signal

# x = 9
#
# pid = os.fork()
#
# def handler(a,b):
#     print("Child done its work")
#
# if pid == 0:
#     time.sleep(5)
#     # os.execlp('ls', 'ls')
#     print("I am a child")
#     print("parent id is", os.getppid())
# else:
#     os.wait()
#     signal.signal(signal.SIGCHLD, handler)
#     print("I am parent", os.getpid())

# fd = os.open("output02.txt", os.O_CREAT|os.O_WRONLY, 644)
fd = os.open("output.txt", os.O_RDONLY)
data = 1
while data:
    data = os.read(fd, 1)
    os.write(1, data)
os.close(fd)
# os.write(fd, b"Hello World\n")

# print(fd)