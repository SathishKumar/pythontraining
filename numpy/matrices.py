import numpy as np

a = np.array([[1,2,3,8],[4,5,6,11]])

b = np.array([[1,2], [4,5], [6,7]])

mat_a = np.matrix(a)

mat_b = np.matrix(b)

print(mat_a)

print(mat_b)

print(mat_b * mat_a)

print(mat_a)