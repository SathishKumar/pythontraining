import numpy as np
import pandas as pd

# arr = np.arange(12).reshape((3,4))
# arr2 = np.arange(12).reshape((3,4))
# print(np.concatenate([arr,  arr2], axis=1))

# s1 = pd.Series([0, 1], index=['a', 'b'])
# s2 = pd.Series([2, 3, 4], index=['c', 'd', 'e'])
# s3 = pd.Series([5, 6], index=['f', 'g'])
# print(pd.concat([s1, s2, s3], sort=False))
# print(pd.concat([s1, s2, s3], axis=1, sort=False))
# s4 = pd.concat([s1 * 5, s3])
# print(pd.concat([s1, s4], axis=1, sort=False))
# print(pd.concat([s1, s4], axis=1, join='inner', sort=False))
# print(pd.concat([s1, s4], axis=1, join_axes=[['a', 'c', 'b', 'e']], sort=False))

a = pd.Series([np.nan, 2.5, np.nan, 3.5, 4.5, np.nan], index=['f','e','d','c','b','a'])

b = pd.Series(np.arange(len(a), dtype=np.float64), index=['f','e','d','c','b','a'])

b[-1] = np.nan

np.where(pd.isnull(a), b, a)
print(b[:-2].combine_first(a[2:]))
# df1 = pd.DataFrame({'a': [1., np.nan, 5., np.nan],
#                  'b': [np.nan, 2., np.nan, 6.],
#                  'c': range(2, 18, 4)})
# df2 = pd.DataFrame({'a': [5., 4., np.nan, 3., 7.],
#                  'b': [np.nan, 3., 4., 6., 8.]})
# print(df1.combine_first(df2))

