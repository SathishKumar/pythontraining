import pandas as pd

import numpy as np

s1 = pd.Series([10,20,30,10,50,60], index=['A','B','C','D','E','F'])
# s1 = pd.Series([10,20,30,40,50,60], index=pd.Index(['A','B','C','D','E','F'], name="Index"))

print(s1.replace([10,20], 100))

print(s1.rename(index=str.lower))