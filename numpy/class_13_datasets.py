import pandas as pd

# read data from the given link and change it to dataset
dfwine = pd.read_csv("https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data",header=None) # If our csv file has no column header then make header = None
#To set column header
dfwine.columns = ['Alcohol type', 'Alcohol', 'Malic acid', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols', 'Flavanoids', 'Nonflavanoid phenols',
 				 'Proanthocyanins', 'Color intensity', 'Hue', 'OD280/OD315 of diluted wines', 'Proline']
print(dfwine.head()) # head to return top 5-6 rows
print(dfwine.iloc[:,1:].values) # To get the data from 1st column as a np array because 0th column is alcohol type


my_df = pd.read_csv("myfile.csv",header=0) # If our csv file has no column header then make header = None
#To set column header
my_df.columns = ['Col1','Col2','Col3','Col4','Col5']
#To set a column as Index number
my_df.set_index('Col5',inplace=True) # inplace to make the changes in the same variable else it will return a new variable
print(my_df.head()) # head to return top 5 rows
print(my_df.iloc[:,:].values) # To get the data from 1st column as a np array because 0th column is header
