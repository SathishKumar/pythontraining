import numpy as np

a1 = np.array([12,-9,10,8,5,-13])

a2 = np.array([-6,17,5,12,-9,11])

a3 = np.array(['a','b','c','d'])
#
# print(a1)
# print(type(a1))
# print(a1.dtype)
print(a1.argsort()) #returns the sorted elements indices
#
#
# if type(a1) is np.ndarray:
#     print("An array")
#
# if a1.dtype == np.int64:
#     print("An array of numbers")

# print("Addition is",a1+a2)
# print("Sub is", a1-a2)
# print("Mul is", a1*a2)
# print("Div is", a1/a2)

# new_arr = np.arange(100, 200)
# select = [10, 25, 6, -5]
# print(new_arr[select])
# div_by_3 = new_arr%3 == 0
# print(div_by_3)
# print(new_arr[div_by_3])
# print(new_arr.sum())
# print(div_by_3.all()) #returns True if all the elements are true.
# print(div_by_3.any()) #returns True if there is atleast one True
# print(div_by_3.sum()) #returns the sum value of all the array elements
# print(div_by_3.nonzero()) # returns the indices of non zero elements

arr = np.array([[3,2], [6,4]])
# print(np.linalg.inv(arr))
print(np.transpose(arr))

print(np.eye(2)) #gives unit matrix
