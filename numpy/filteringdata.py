import pandas as pd
from numpy import nan as NA

data = pd.Series([1,2,NA,NA,7,NA])

print(data.dropna())

print(data[data.notnull()])

