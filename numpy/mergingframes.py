import pandas as pd

# df1 = pd.DataFrame({'lkey' : ['a','b','d'],
#                     'data1' : range(3)})
#
# df2 = pd.DataFrame({'rkey' : ['a','c','a','b','c','c','b'],
#                     'data2' : range(7)})
#
# print(pd.merge(df1, df2, left_on='lkey', right_on='rkey')) #default inner
# print(pd.merge(df1, df2, left_on='lkey', right_on='rkey', how='inner'))
# print(pd.merge(df1, df2, left_on='lkey', right_on='rkey', how='outer'))

left = pd.DataFrame({'st':['KA', 'KA', 'KL'],
                     'fg':['pd', 'wh', 'pd'],
                     'qty':[12,11,14]})

right = pd.DataFrame({'st':['KA', 'KA', 'KL', 'KL'],
                     'fg':['pd', 'pd', 'pd', 'cn'],
                     'qty':[12,11,14, 20]})

print(pd.merge(left, right, on=['fg','st'], how="outer", suffixes=('_left', '_right')))
print(pd.merge(left, right, on=['fg','st'], how="left", suffixes=('_left', '_right')))
print(pd.merge(left, right, on=['fg','st'], how="right", suffixes=('_left', '_right')))