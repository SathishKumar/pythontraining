# import pandas as pd
#
# ages = [10,20,22,45,21,18,24,23,41,45]
#
# bins = [8,15,30,40,50]
#
# group_names = ['child','youth','middle-age','senior']
#
# cats = pd.cut(ages,bins,labels=group_names)
#
# print(cats)
#
# print(cats.codes)
#
# print(pd.value_counts(cats))


from pandas import DataFrame
import numpy as np

frame = DataFrame(np.arange(12).reshape((4,3)), index=[['a','a','b','a'],[1,2,1,2]],
                  columns=[['Ohio', 'Ohio', 'Colorodo'], ['green','red','green']])

frame.index.names = ['key1', 'key2']
frame.columns.names = ['state','color']
# print(frame)
print(frame.swaplevel('key1','key2'))

print(frame.sort_index(level=0))
