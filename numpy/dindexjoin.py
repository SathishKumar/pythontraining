import pandas as pd
import numpy as np

'''
Reshaping the hierarchial index
'''

df1 = pd.DataFrame({'key1':['MH','MH','MH', 'TN', 'TN'],
                    'key2' : [2000, 2001, 2002, 2001, 2002],
                    'data':[3,2,5,4,6]})

df2 = pd.DataFrame(np.arange(12).reshape((6,2)),
                   index=pd.Index(['KA','KL','AP', 'MP', 'UP', 'MH'],[2000, 2001, 2002, 2001, 2002,2000],name='state'),
                   columns=pd.Index(['event1', 'event2'], name='event'))

# print(df2.loc['MH'].loc[2000])

# print(pd.merge(df1, df2, left_on=['key1', 'key2'], right_index=True))
# print(df2)

# print(df2.stack())

sdf2 = df2.stack()

print(sdf2.unstack(0))

print(sdf2.unstack(1))



