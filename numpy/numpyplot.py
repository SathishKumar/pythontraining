import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# s = pd.Series(np.random.randn(10).cumsum(),index=np.arange(0,100,10))
df = pd.DataFrame({'a':[10,20,30,40],'cols':[1,2,3,4], 'qty':[20,10,5,55]}, index=[1,2,3,4])
df.plot()

plt.show()