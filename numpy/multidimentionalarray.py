import numpy as np

a34 = np.array([[12,10,-21,7],
                [-17, 18, 19, 16],
                [13, -7, -18, 18]])

print(a34)

print(a34.size)

print(a34.shape)

print("Second row is", a34[1])

print("First column is", a34[:,3])

print(a34[1, 1:3])

x = np.arange(0,10,1)

print(x)

ls = np.linspace(0, 10, 25)

print(ls)

print(ls.shape)

print(np.ones((3,3)))