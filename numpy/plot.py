import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-np.pi, np.pi, 256)

y1 = np.sin(x)
y2 = np.cos(x)

plt.figure(figsize=(6,4), dpi=60)
plt.plot(x, y1, color = 'blue', linewidth=5, linestyle="-.", label="sin")
plt.plot(x, y2, color = 'orange', linewidth=2.5, label="cos")
# plt.xlim(x.min()*2)
plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi], [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$\pi/2$', r'$\pi$'], rotation=30)

axis = plt.gca()
axis.spines['right'].set_color(None)
axis.spines['top'].set_color(None)
axis.spines['left'].set_position(('data', 0))
axis.spines['bottom'].set_position(('data', 0))
# axis.xaxis.set_ticks_position('top')
# axis.yaxis.set_ticks_position('right')
for ax in axis.get_xticklabels() + axis.get_yticklabels():
    ax.set_fontsize(16)
    ax.set_bbox(dict(
        facecolor = 'grey',
        edgecolor = 'red',
        alpha = 0.35
    ))

plt.legend(loc="best")
plt.show()


