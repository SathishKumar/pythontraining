import pandas as pd
import  numpy as np
#
# obj = pd.Series(['pd', 'wh', 'sc'], index=[2,4,10])
# # print(obj.reindex(range(11), method='bfill', limit=2))
# print(obj.drop(2))
#
# df1 = pd.DataFrame(np.arange(9).reshape((3,3)), index=['a','b','c'], columns=['Ohio', 'Texas', 'California'])
# df2 = df1.reindex(['a', 'b', 'c', 'd'])
# states = ['Texas', 'Utah', 'California']
# print(df1.drop('Texas', axis=1))
# print(df1.reindex(index=['a','b','c','d'], method='ffill'))

obj = pd.Series([7,8,3,2,1,-2])

print(obj.rank())