import pandas as pd
import numpy as np

df1 = pd.DataFrame({'key1':['MH','MH','MH', 'TN', 'TN'],
                    'key2' : [2000, 2001, 2002, 2001, 2002],
                    'data':[3,2,5,4,6]})
df2 = pd.DataFrame(np.arange(12).reshape((6,2)),
                   index=[['MH','MH','MH', 'TN', 'TN', 'MH'],[2000, 2001, 2002, 2001, 2002, 2000]],
                   columns=['event1', 'event2'])

# print(df2.loc['MH'].loc[2000])
# print(pd.merge(df1, df2, left_on=['key1', 'key2'], right_index=True))
# print(df2.drop_duplicates())

df = pd.DataFrame({
                'spice' : ['tr', 'cd', 'cu', 'tr', 'tr', 'cu'],
                'brand' : ['pathanjali', 'P&D', 'NB', 'pathanjali', 'NB', 'P&D'],
                'price' : [12, 22, 10, 12, 12, 10],
                'qty' : [500, 100, 200, 600, 700, 200]
                })


print(df.groupby(df['spice']).mean())
print(dict(df.groupby(df['spice']).size()))


# for name, group in df.groupby('spice'):
#     print(name + " -> " + str(group))