import os
import sys

def file_write(fname, msg):
    f = open(fname, 'w')
    msg = list(map(lambda x:str(x)+"\n", msg))
    f.writelines(msg)
    f.close()

def file_read(fname):
    f = open(fname)
    data = f.readlines()
    data = list(map(lambda x:int(x.strip()), data))
    f.close()
    print(data)

def file_append(fname, msg):
    f = open(fname, 'a')
    f.write(msg+"\n")
    f.close()

def dict_write(fname, msg):
    f = open(fname, 'w')
    for (i, j) in msg.items():
        f.write(str(i) + "," + '-'.join(map(lambda x: str(x), j)) + "\n")

    f.close()

def dict_read(fname):
    f = open(fname)
    lines = list((map(lambda x: tuple((x.strip()).split(',')), f.readlines())))
    d = {}
    for (i,j) in lines:
        d.update({i:list(map(lambda x: int(x), j.split('-')))})

    print(d)

def read_tables(fname):
    if not os.path.exists(fname):
        sys.stderr.write("File doesnot exists")
        sys.exit(4)
    import numpy as np
    f = open(fname)
    lines = list((map(lambda x: tuple((x.strip()).split(' ')), f.readlines())))
    print(lines)
    content_arr = np.array(lines[1:])
    print(content_arr)
    content_list = (content_arr.T)
    print(content_list)
    # data = dict(zip(lines(0), content_list))


if __name__ == '__main__':
    # file_write("new.txt", [1,2,3,4])
    # dict_write("new.txt", dict(a=10, b=20, c=30, d=40))
    a = {'a':[1,2,3], 'b':[10,20,30,40]}
    # dict_write("new.txt", a)
    # dict_read("new.txt")
    read_tables("new.txt")
