import httplib2

c = httplib2.HTTPConnectionWithTimeout("www.gnu.org")
c.putrequest("GET", "")
c.putheader('someheader', 'somevalue')
c.endheaders()

r = c.getresponse()
data = r.read()

print(str(data, 'UTF-8'))

c.close()
