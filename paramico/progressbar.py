from tqdm import tqdm
import sys
import time
import paramiko

def viewbar(a,b):
    res = a/int(b)*100
    sys.stdout.write('\rComplete percent: %.2f %%' % (res))
    sys.stdout.flush()

def viewbar2(a,b):
    pbar = tqdm(total=int(b), unit='b',unit_scale=True)
    pbar.update(a)

t = paramiko.Transport(('127.0.0.1', 22))
t.connect(username='cisco', password='cisco')
sftp = paramiko.SFTPClient.from_transport(t)
sftp.put('/home/cisco/Downloads/Anaconda3-5.3.0-Linux-x86_64.sh', '/home/cisco/Downloads/getting_anaconda/getting_anaconda.sh', callback=viewbar2)

t.close()