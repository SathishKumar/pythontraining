from BasicHttpServer import HTTPServer, CGIHTTPRequestHandler

class helloHandler(CGIHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/hello':
            self.send_response(200, "OK")
            self.send_header("content-type","text/html")
            self.end_headers()
            self.wfile.write(b"""<html><head><title>Hello</title></head><body><h2>Hello World</h2></body></html>""")

serv = HTTPServer(("", 8081), helloHandler)
print("server running..")
serv.serve_forever()