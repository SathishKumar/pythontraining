import os
import paramiko
from ftptransport import DownloadFile, ConvertIntoCaps

def LogMEas(func):

    def LogALL(*args):
        import logging
        LOG_FILENAME = 'logging_example.out'
        logging.basicConfig(
            filename=LOG_FILENAME,
            level=logging.DEBUG,
        )
        func(*args)
        logging.debug(func.__name__)

        with open(LOG_FILENAME, 'rt') as f:
            body = f.read()

        print('FILE:')
        print(body)
    return LogALL

@LogMEas
def printTotals(transferred, toBeTransferred):
    print("Transferred: {0}\tOut of: {1}".format(transferred, toBeTransferred))

@LogMEas
def sendFileTo(host, port, source, dest):
    from tqdm import tqdm
    DownloadFile()
    ConvertIntoCaps("README")
    def viewbar(a, b):
        res = a / int(b) * 100
        sys.stdout.write('\rComplete percent: %.2f %%' % (res))
        sys.stdout.flush()

    def viewbar2(a, b):
        pbar = tqdm(total=int(b), unit='b', unit_scale=True)
        pbar.update(a)

    t = paramiko.Transport((host, port))
    t.connect(username='cisco', password='cisco')
    sftp = paramiko.SFTPClient.from_transport(t)
    sftp.put(source, dest, callback=viewbar2)

    t.close()

if __name__ == '__main__':

    sendFileTo("localhost", 22, "/home/cisco/Desktop/NewReadME.txt" ,"/home/cisco/Desktop/getreadme/NewReadMe.txt")
