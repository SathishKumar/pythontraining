wage = int(input("Enter the wage: "))

"""
if wage < 1000:
    print("Wage is low")
elif wage < 2000:
    print("Wage is moderate")
else:
    print("Wage is high")
"""

while wage < 10:
    wage = wage + 1
    if wage <= 4:
        print("Continue..")
        continue
    print("Wage is ", wage)
    if wage >= 8:
        print("Break")
        break

for i in range(10):
    print("i is ", i)

a = {'a':10, 'b':20, 'c':30,'d':40}

for (i,j) in a.items():
    print(i," => ", j)


