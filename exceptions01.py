#
#
# import sys
#
# try:
#     f = open('newkfd.txt')
#     s = f.readline()
#     i = int(s.strip())
#
# # except FileNotFoundError:
# #     print("No such file")
#
# except ValueError:
#     print("Not a number")
#
# except:
#     print("Unexpected Error:", sys.exc_info()[0])
#     raise

import sys

class MyExcept(Exception):
    def __init__(self):
        return
    def __str__(self):
        print("My except occurs")
        return "My Except occurs"

def myfunc():
    raise MyExcept

# try:
#     myfunc()
# except:
#     raise

def divide():
    x = int(input("Enter divisor: "))
    y = int(input("Enter dividend: "))
    return y/x

try:
    a = divide()
except ZeroDivisionError:
    print("Cannot divide by 0")
    raise
except ValueError:
    print("Not a number")
except:
    print(sys.exc_info()[0])
    raise
else:
    a += 100
    print(a)
finally:
    print("Bye!")

print("I am here")
# try:
#     raise Exception('spam', 'eggs')
# except Exception as inst:
#     print(inst)

