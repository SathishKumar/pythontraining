from classes import Student

class Cs_student(Student):

    def __init__(self, n, a, s):
        Student.__init__(self, n, a)
        self.section = s

    def get_age(self):
        print("Age is", Student.get_age(self))



s1 = Cs_student('Ragul', 20, 1)

s2 = Cs_student('Ragul', 22, 1)

s1.get_age()

print(s1 > s2)
