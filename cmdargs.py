import sys

# for fname in sys.argv[1:]:
#     try:
#         f = open(fname, 'r')
#     except IOError:
#         print("Cannot open", fname)
#     else:
#         print(fname + " -> " + str(len(f.readlines())) + " lines")
#         f.close()

try:
    f = open(sys.argv[1], 'r')
except IOError:
    print("File does not exists")
else:
    for i in f.readlines():
        print(i)
    f.close()

