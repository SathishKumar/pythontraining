# import sys
#
# sys.path.append('/home/cisco/PycharmProjects/newprogram/lib')

# import newfile
#
# print(newfile.add(10,5))

from classes import Student
from copy import deepcopy

student1 = Student('Ragul', 23)
# student2 = Student('Mani', 22)
# student3 = Student('Gokul', 22)
#
# # student3 = deepcopy(student1)
#
# print(student2.get_name() + "'s age is", student2.get_age())
#
# print(student1.get_name() + "'s age is incremented", student1.get_age())
#
# student1.inc_age(10)
#
# print(student2.get_name() + "'s age is", student2.get_age())
#
# print(student1.get_name() + "'s age is incremented", student1.get_age())
#
# print(student3.get_roll())

import pickle
f = open("student.bin", "wb")
pickle.dump(student1, f)
f.close()

f = open("student.bin", "rb")
stu6 = pickle.load(f)

print(stu6.get_name())
