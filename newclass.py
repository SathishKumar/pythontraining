class Counter:

    overall_total = 0

    def __init__(self):
        self.total = 0

    def increment(self):
        Counter.overall_total += 1
        self.total += 1


c1 = Counter()

c2 = Counter()

c1.increment()

c2.increment()

c2.increment()

print(c1.overall_total)
print(c1.total)
print(c2.total)
print(c2.overall_total)

